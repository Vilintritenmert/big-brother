USE test;

CREATE table geo_location_models (
  ip varchar(15),
  country varchar(50),
  city varchar (50),
  UNIQUE KEY (ip)
) ENGINE = MYISAM;