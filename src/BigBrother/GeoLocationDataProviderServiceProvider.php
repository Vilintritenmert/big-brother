<?php
declare(strict_types = 1);

namespace BigBrother;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class GeoLocationDataProviderServiceProvider implements ServiceProviderInterface {

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $app A container instance
     */
    public function register(Container $app)
    {
        $app['geoLocationDataProvider'] = $app->protect(function () use ($app) {
            return new GeoLocationDataProviderIpInfo(
                $app['dataProvider.token']
            );
        });
    }
}