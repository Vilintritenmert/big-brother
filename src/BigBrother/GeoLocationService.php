<?php
declare(strict_types = 1);

namespace BigBrother;

class GeoLocationService {
    /**
     * @var GeoLocationRepositoryInterface
     */
    private $geoLocationRepository;

    /**
     * @var GeoLocationDataProviderInterface
     */
    private $geoLocationDataProvider;

    /**
     * GeoLocationService constructor.
     *
     * @param GeoLocationRepositoryInterface   $geoLocationRepository
     * @param GeoLocationDataProviderInterface $geoLocationDataProvider
     */
    public function __construct(
        GeoLocationRepositoryInterface $geoLocationRepository,
        GeoLocationDataProviderInterface $geoLocationDataProvider
    ) {
        $this->geoLocationRepository = $geoLocationRepository;
        $this->geoLocationDataProvider = $geoLocationDataProvider;
    }

    /**
     * @param Ip $ip
     *
     * @return GeoLocationResponse
     * @throws \Exception
     */
    public function getDataByIp(Ip $ip) : GeoLocationResponse
    {
        $cachedResponse = $this->geoLocationRepository->findByIp($ip);

        if ($cachedResponse) {
            return $cachedResponse;
        }

        $remoteResponse = $this->geoLocationDataProvider->findByIp($ip);
        $this->geoLocationRepository->store($ip, $remoteResponse);

        return $remoteResponse;
    }

}