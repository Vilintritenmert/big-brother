<?php
declare(strict_types = 1);

namespace BigBrother;

use BigBrother\Models\GeoLocationModel;

class GeoLocationRepositoryActiveRecord implements GeoLocationRepositoryInterface {

    /**
     * GeoLocationRepositoryActiveRecord constructor.
     *
     * @param string $host
     * @param int $port
     * @param string $login
     * @param string $password
     * @param string $databaseName
     */
    public function __construct(
        string $host,
        int $port,
        string $login,
        string $password,
        string $databaseName
    ) {
        \ActiveRecord\Config::initialize(function($cfg) use ($host, $port, $login, $password, $databaseName)
        {
            $cfg->set_model_directory(__DIR__ . '/Models');
            $cfg->set_connections(array('development' => "mysql://{$login}:{$password}@{$host}:{$port}/{$databaseName}"));
        });
    }

    /**
     * @param Ip $ip
     *
     * @return GeoLocationResponse|null
     */
    public function findByIp(Ip $ip) : ?GeoLocationResponse
    {
        $geoLocationActiveRecord = GeoLocationModel::find_by_ip($ip->getIp());

        if (!$geoLocationActiveRecord) {
            return null;
        }

        return new GeoLocationResponse(
            $geoLocationActiveRecord->country,
            $geoLocationActiveRecord->city
        );
    }


    /**
     * @param Ip                  $ip
     * @param GeoLocationResponse $response
     */
    public function store(Ip $ip, GeoLocationResponse $response)
    {
        GeoLocationModel::create([
           'ip' => $ip->getIp(),
           'country' => $response->getCountry(),
           'city' => $response->getCity()
        ]);
    }
}