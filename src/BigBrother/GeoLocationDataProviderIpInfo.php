<?php
declare(strict_types=1);

namespace BigBrother;


class GeoLocationDataProviderIpInfo implements GeoLocationDataProviderInterface
{
    /**
     * @var string|string
     */
    private $token;

    /**
     * GeoLocationRepositoryIpInfo constructor.
     *
     * @param string $token
     */
    public function __construct(
        string $token
    ) {
        $this->token = $token;
    }

    /**
     * @param Ip $ip
     *
     * @return GeoLocationResponse
     * @throws \Exception
     */
    public function findByIp(Ip $ip) : GeoLocationResponse
    {
        $uri = "https://ipinfo.io/{$ip->getIp()}?TOKEN={$this->token}";

        $apiResponse = @file_get_contents($uri);

        if (!$apiResponse) {
            throw new \Exception('Problem with connection');
        }

        $decodedResponse = json_decode($apiResponse, true);

        $country = array_key_exists('country', $decodedResponse) ? $decodedResponse['country'] : 'UNKNOWN';
        $city = array_key_exists('city', $decodedResponse) ? $decodedResponse['city'] : 'UNKNOWN';

        return new GeoLocationResponse(
            $country,
            $city
        );
    }
}