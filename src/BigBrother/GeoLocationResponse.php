<?php
declare(strict_types = 1);

namespace BigBrother;

class GeoLocationResponse {

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $city;

    /**
     * GeoLocationResponse constructor.
     *
     * @param string $country
     * @param string $city
     */
    public function __construct(
        string $country,
        string $city
    ) {
        $this->setCountry($country);
        $this->setCity($city);
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }


}