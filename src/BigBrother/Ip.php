<?php
declare(strict_types = 1);

namespace BigBrother;

class Ip {

    /**
     * @var string|string
     */
    private $ip;


    /**
     * Ip constructor.
     *
     * @param string $ip
     */
    public function __construct(string $ip)
    {
        $this->setIp($ip);
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getIp();
    }

}