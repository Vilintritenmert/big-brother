<?php
declare(strict_types = 1);

namespace BigBrother;


// TODO: Change name
interface GeoLocationDataProviderInterface {

    public function findByIp(Ip $ip) : GeoLocationResponse;

}