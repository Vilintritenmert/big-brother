<?php
declare(strict_types = 1);

namespace BigBrother;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class GeoLocationServiceServiceProvider implements ServiceProviderInterface {

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $app A container instance
     *
     * @throws \Exception
     */
    public function register(Container $app)
    {
        if (!$app['geoLocationRepository']) {
            throw new \Exception('Required GeoLocationRepository first');
        }

        if (!$app['geoLocationDataProvider']) {
            throw new \Exception('Required GeoLocationRepository first');
        }

        $app['geoLocationService']  = $app->protect(function () use ($app) {
            return new GeoLocationService(
                $app['geoLocationRepository'](),
                $app['geoLocationDataProvider']()
            );
        });
    }
}