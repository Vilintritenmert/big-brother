<?php
declare(strict_types = 1);

namespace BigBrother;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class GeoLocationRepositoryServiceProvider implements ServiceProviderInterface {

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $app A container instance
     */
    public function register(Container $app)
    {
        $app['geoLocationRepository'] = $app->protect(function () use ($app) {
            return new GeoLocationRepositoryActiveRecord(
                $app['database.host'],
                $app['database.port'],
                $app['database.login'],
                $app['database.password'],
                $app['database.name']
            );
        });
    }
}