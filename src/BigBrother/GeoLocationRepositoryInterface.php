<?php
declare(strict_types = 1);

namespace BigBrother;

interface GeoLocationRepositoryInterface {

    public function findByIp(Ip $ip) : ?GeoLocationResponse;

    public function store(Ip $ip, GeoLocationResponse $response);

}