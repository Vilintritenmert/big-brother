<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    return $twig;
});

$dotenv = new Symfony\Component\Dotenv\Dotenv();
$dotenv->load(__DIR__.'/../.env');

$app->register(new \BigBrother\GeoLocationRepositoryServiceProvider(), [
    'database.host' => 'big_brother_database',
    'database.port' => (int)getenv('DB_PORT'),
    'database.login' => getenv('DB_USERNAME'),
    'database.password' => getenv('DB_PASSWORD'),
    'database.name' => getenv('DB_DATABASE'),
]);

$app->register(new \BigBrother\GeoLocationDataProviderServiceProvider(), [
    'dataProvider.token' => getenv('IP_INFO_TOKEN')
]);

$app->register(new \BigBrother\GeoLocationServiceServiceProvider());

return $app;
